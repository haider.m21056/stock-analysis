import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import axios from 'axios';
import CurrencyConversion from './CurrencyConversion';


jest.mock('axios');


const mockExchangeRates = {
  USD: 1,
  EUR: 0.85,
  GBP: 0.75,
 
};


axios.get.mockResolvedValue({
  data: {
    rates: mockExchangeRates,
  },
});

test('CurrencyConversion component renders correctly', async () => {
  render(<CurrencyConversion />);


  expect(screen.getByText('Currency Conversion')).toBeInTheDocument();


  await waitFor(() => expect(axios.get).toHaveBeenCalledWith('https://api.exchangerate-api.com/v4/latest/USD'));
});

test('CurrencyConversion component handles currency conversion', async () => {
  render(<CurrencyConversion />);

 
  fireEvent.change(screen.getByLabelText('Source Currency'), { target: { value: 'USD' } });


  fireEvent.change(screen.getByLabelText('Target Currency'), { target: { value: 'EUR' } });


  fireEvent.change(screen.getByLabelText('Amount'), { target: { value: '100' } });

  
  fireEvent.click(screen.getByText('Convert'));


  await waitFor(() => {
    expect(screen.getByText('Value: 85.00 EUR')).toBeInTheDocument();
  });
});
// ... (imports and mocks)

test('CurrencyConversion component renders correctly', async () => {
  render(<CurrencyConversion />);

  // Check if the component renders properly
  expect(screen.getByText('Currency Conversion')).toBeInTheDocument();

  // Mock API request should have been called during component rendering
  await waitFor(() => {
    expect(axios.get).toHaveBeenCalledWith('https://api.exchangerate-api.com/v4/latest/USD');
  }, { timeout: 5000, interval: 1000 }); // Adjust timeout and interval as needed
});

test('CurrencyConversion component handles currency conversion', async () => {
  render(<CurrencyConversion />);

  // Select source currency
  fireEvent.change(screen.getByLabelText('Source Currency'), { target: { value: 'USD' } });

  // Select target currency
  fireEvent.change(screen.getByLabelText('Target Currency'), { target: { value: 'EUR' } });

  // Enter amount
  fireEvent.change(screen.getByLabelText('Amount'), { target: { value: '100' } });

  // Click on Convert button
  fireEvent.click(screen.getByText('Convert'));

  // Wait for the result to appear
  await waitFor(() => {
    expect(screen.getByText('Value: 85.00 EUR')).toBeInTheDocument();
  }, { timeout: 5000, interval: 1000 }); // Adjust timeout and interval as needed
});
